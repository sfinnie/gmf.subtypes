/**
 */
package Model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association Class Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Model.AssociationClassLink#getCls <em>Cls</em>}</li>
 *   <li>{@link Model.AssociationClassLink#getAssoc <em>Assoc</em>}</li>
 * </ul>
 * </p>
 *
 * @see Model.ModelPackage#getAssociationClassLink()
 * @model
 * @generated
 */
public interface AssociationClassLink extends DomainElement {
	/**
	 * Returns the value of the '<em><b>Cls</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cls</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cls</em>' reference.
	 * @see #setCls(Model.Class)
	 * @see Model.ModelPackage#getAssociationClassLink_Cls()
	 * @model
	 * @generated
	 */
	Model.Class getCls();

	/**
	 * Sets the value of the '{@link Model.AssociationClassLink#getCls <em>Cls</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cls</em>' reference.
	 * @see #getCls()
	 * @generated
	 */
	void setCls(Model.Class value);

	/**
	 * Returns the value of the '<em><b>Assoc</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assoc</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assoc</em>' reference.
	 * @see #setAssoc(Association)
	 * @see Model.ModelPackage#getAssociationClassLink_Assoc()
	 * @model
	 * @generated
	 */
	Association getAssoc();

	/**
	 * Sets the value of the '{@link Model.AssociationClassLink#getAssoc <em>Assoc</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assoc</em>' reference.
	 * @see #getAssoc()
	 * @generated
	 */
	void setAssoc(Association value);

} // AssociationClassLink
