/**
 */
package Model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Model.ModelPackage#getClass_()
 * @model
 * @generated
 */
public interface Class extends DomainElement {
} // Class
