/**
 */
package Model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Model.Association#getParticipantA <em>Participant A</em>}</li>
 *   <li>{@link Model.Association#getParticipantB <em>Participant B</em>}</li>
 * </ul>
 * </p>
 *
 * @see Model.ModelPackage#getAssociation()
 * @model
 * @generated
 */
public interface Association extends DomainElement {
	/**
	 * Returns the value of the '<em><b>Participant A</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participant A</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participant A</em>' reference.
	 * @see #setParticipantA(Model.Class)
	 * @see Model.ModelPackage#getAssociation_ParticipantA()
	 * @model required="true"
	 * @generated
	 */
	Model.Class getParticipantA();

	/**
	 * Sets the value of the '{@link Model.Association#getParticipantA <em>Participant A</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Participant A</em>' reference.
	 * @see #getParticipantA()
	 * @generated
	 */
	void setParticipantA(Model.Class value);

	/**
	 * Returns the value of the '<em><b>Participant B</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participant B</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participant B</em>' reference.
	 * @see #setParticipantB(Model.Class)
	 * @see Model.ModelPackage#getAssociation_ParticipantB()
	 * @model required="true"
	 * @generated
	 */
	Model.Class getParticipantB();

	/**
	 * Sets the value of the '{@link Model.Association#getParticipantB <em>Participant B</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Participant B</em>' reference.
	 * @see #getParticipantB()
	 * @generated
	 */
	void setParticipantB(Model.Class value);

} // Association
