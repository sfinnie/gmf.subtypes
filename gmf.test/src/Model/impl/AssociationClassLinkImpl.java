/**
 */
package Model.impl;

import Model.Association;
import Model.AssociationClassLink;
import Model.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association Class Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Model.impl.AssociationClassLinkImpl#getCls <em>Cls</em>}</li>
 *   <li>{@link Model.impl.AssociationClassLinkImpl#getAssoc <em>Assoc</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssociationClassLinkImpl extends DomainElementImpl implements AssociationClassLink {
	/**
	 * The cached value of the '{@link #getCls() <em>Cls</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCls()
	 * @generated
	 * @ordered
	 */
	protected Model.Class cls;

	/**
	 * The cached value of the '{@link #getAssoc() <em>Assoc</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssoc()
	 * @generated
	 * @ordered
	 */
	protected Association assoc;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationClassLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.ASSOCIATION_CLASS_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model.Class getCls() {
		if (cls != null && cls.eIsProxy()) {
			InternalEObject oldCls = (InternalEObject)cls;
			cls = (Model.Class)eResolveProxy(oldCls);
			if (cls != oldCls) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModelPackage.ASSOCIATION_CLASS_LINK__CLS, oldCls, cls));
			}
		}
		return cls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model.Class basicGetCls() {
		return cls;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCls(Model.Class newCls) {
		Model.Class oldCls = cls;
		cls = newCls;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ASSOCIATION_CLASS_LINK__CLS, oldCls, cls));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association getAssoc() {
		if (assoc != null && assoc.eIsProxy()) {
			InternalEObject oldAssoc = (InternalEObject)assoc;
			assoc = (Association)eResolveProxy(oldAssoc);
			if (assoc != oldAssoc) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModelPackage.ASSOCIATION_CLASS_LINK__ASSOC, oldAssoc, assoc));
			}
		}
		return assoc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Association basicGetAssoc() {
		return assoc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssoc(Association newAssoc) {
		Association oldAssoc = assoc;
		assoc = newAssoc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ASSOCIATION_CLASS_LINK__ASSOC, oldAssoc, assoc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.ASSOCIATION_CLASS_LINK__CLS:
				if (resolve) return getCls();
				return basicGetCls();
			case ModelPackage.ASSOCIATION_CLASS_LINK__ASSOC:
				if (resolve) return getAssoc();
				return basicGetAssoc();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.ASSOCIATION_CLASS_LINK__CLS:
				setCls((Model.Class)newValue);
				return;
			case ModelPackage.ASSOCIATION_CLASS_LINK__ASSOC:
				setAssoc((Association)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.ASSOCIATION_CLASS_LINK__CLS:
				setCls((Model.Class)null);
				return;
			case ModelPackage.ASSOCIATION_CLASS_LINK__ASSOC:
				setAssoc((Association)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.ASSOCIATION_CLASS_LINK__CLS:
				return cls != null;
			case ModelPackage.ASSOCIATION_CLASS_LINK__ASSOC:
				return assoc != null;
		}
		return super.eIsSet(featureID);
	}

} //AssociationClassLinkImpl
