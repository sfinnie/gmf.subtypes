/**
 */
package Model.impl;

import Model.Association;
import Model.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Model.impl.AssociationImpl#getParticipantA <em>Participant A</em>}</li>
 *   <li>{@link Model.impl.AssociationImpl#getParticipantB <em>Participant B</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssociationImpl extends DomainElementImpl implements Association {
	/**
	 * The cached value of the '{@link #getParticipantA() <em>Participant A</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipantA()
	 * @generated
	 * @ordered
	 */
	protected Model.Class participantA;

	/**
	 * The cached value of the '{@link #getParticipantB() <em>Participant B</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipantB()
	 * @generated
	 * @ordered
	 */
	protected Model.Class participantB;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.ASSOCIATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model.Class getParticipantA() {
		if (participantA != null && participantA.eIsProxy()) {
			InternalEObject oldParticipantA = (InternalEObject)participantA;
			participantA = (Model.Class)eResolveProxy(oldParticipantA);
			if (participantA != oldParticipantA) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModelPackage.ASSOCIATION__PARTICIPANT_A, oldParticipantA, participantA));
			}
		}
		return participantA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model.Class basicGetParticipantA() {
		return participantA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParticipantA(Model.Class newParticipantA) {
		Model.Class oldParticipantA = participantA;
		participantA = newParticipantA;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ASSOCIATION__PARTICIPANT_A, oldParticipantA, participantA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model.Class getParticipantB() {
		if (participantB != null && participantB.eIsProxy()) {
			InternalEObject oldParticipantB = (InternalEObject)participantB;
			participantB = (Model.Class)eResolveProxy(oldParticipantB);
			if (participantB != oldParticipantB) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModelPackage.ASSOCIATION__PARTICIPANT_B, oldParticipantB, participantB));
			}
		}
		return participantB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model.Class basicGetParticipantB() {
		return participantB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParticipantB(Model.Class newParticipantB) {
		Model.Class oldParticipantB = participantB;
		participantB = newParticipantB;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ASSOCIATION__PARTICIPANT_B, oldParticipantB, participantB));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.ASSOCIATION__PARTICIPANT_A:
				if (resolve) return getParticipantA();
				return basicGetParticipantA();
			case ModelPackage.ASSOCIATION__PARTICIPANT_B:
				if (resolve) return getParticipantB();
				return basicGetParticipantB();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.ASSOCIATION__PARTICIPANT_A:
				setParticipantA((Model.Class)newValue);
				return;
			case ModelPackage.ASSOCIATION__PARTICIPANT_B:
				setParticipantB((Model.Class)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.ASSOCIATION__PARTICIPANT_A:
				setParticipantA((Model.Class)null);
				return;
			case ModelPackage.ASSOCIATION__PARTICIPANT_B:
				setParticipantB((Model.Class)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.ASSOCIATION__PARTICIPANT_A:
				return participantA != null;
			case ModelPackage.ASSOCIATION__PARTICIPANT_B:
				return participantB != null;
		}
		return super.eIsSet(featureID);
	}

} //AssociationImpl
