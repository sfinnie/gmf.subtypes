package Model.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;

/**
 * @generated
 */
public class TestDiagramUpdater {

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestNodeDescriptor> getSemanticChildren(
			View view) {
		switch (Model.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Model.diagram.edit.parts.DomainEditPart.VISUAL_ID:
			return getDomain_1000SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestNodeDescriptor> getDomain_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		Model.Domain modelElement = (Model.Domain) view.getElement();
		LinkedList<Model.diagram.part.TestNodeDescriptor> result = new LinkedList<Model.diagram.part.TestNodeDescriptor>();
		for (Iterator<?> it = modelElement.getDomainElements().iterator(); it
				.hasNext();) {
			Model.DomainElement childElement = (Model.DomainElement) it.next();
			int visualID = Model.diagram.part.TestVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == Model.diagram.edit.parts.ClassEditPart.VISUAL_ID) {
				result.add(new Model.diagram.part.TestNodeDescriptor(
						childElement, visualID));
				continue;
			}
			if (visualID == Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID) {
				result.add(new Model.diagram.part.TestNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestLinkDescriptor> getContainedLinks(
			View view) {
		switch (Model.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Model.diagram.edit.parts.DomainEditPart.VISUAL_ID:
			return getDomain_1000ContainedLinks(view);
		case Model.diagram.edit.parts.ClassEditPart.VISUAL_ID:
			return getClass_2001ContainedLinks(view);
		case Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID:
			return getGenSpec_2002ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestLinkDescriptor> getIncomingLinks(
			View view) {
		switch (Model.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Model.diagram.edit.parts.ClassEditPart.VISUAL_ID:
			return getClass_2001IncomingLinks(view);
		case Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID:
			return getGenSpec_2002IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestLinkDescriptor> getOutgoingLinks(
			View view) {
		switch (Model.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Model.diagram.edit.parts.ClassEditPart.VISUAL_ID:
			return getClass_2001OutgoingLinks(view);
		case Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID:
			return getGenSpec_2002OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestLinkDescriptor> getDomain_1000ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestLinkDescriptor> getClass_2001ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestLinkDescriptor> getGenSpec_2002ContainedLinks(
			View view) {
		Model.GenSpec modelElement = (Model.GenSpec) view.getElement();
		LinkedList<Model.diagram.part.TestLinkDescriptor> result = new LinkedList<Model.diagram.part.TestLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_GenSpec_Supertype_4002(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_GenSpec_Subtype_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestLinkDescriptor> getClass_2001IncomingLinks(
			View view) {
		Model.Class modelElement = (Model.Class) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<Model.diagram.part.TestLinkDescriptor> result = new LinkedList<Model.diagram.part.TestLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_GenSpec_Supertype_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_GenSpec_Subtype_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestLinkDescriptor> getGenSpec_2002IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestLinkDescriptor> getClass_2001OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<Model.diagram.part.TestLinkDescriptor> getGenSpec_2002OutgoingLinks(
			View view) {
		Model.GenSpec modelElement = (Model.GenSpec) view.getElement();
		LinkedList<Model.diagram.part.TestLinkDescriptor> result = new LinkedList<Model.diagram.part.TestLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_GenSpec_Supertype_4002(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_GenSpec_Subtype_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<Model.diagram.part.TestLinkDescriptor> getIncomingFeatureModelFacetLinks_GenSpec_Supertype_4002(
			Model.Class target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<Model.diagram.part.TestLinkDescriptor> result = new LinkedList<Model.diagram.part.TestLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == Model.ModelPackage.eINSTANCE
					.getGenSpec_Supertype()) {
				result.add(new Model.diagram.part.TestLinkDescriptor(
						setting.getEObject(),
						target,
						Model.diagram.providers.TestElementTypes.GenSpecSupertype_4002,
						Model.diagram.edit.parts.GenSpecSupertypeEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<Model.diagram.part.TestLinkDescriptor> getIncomingFeatureModelFacetLinks_GenSpec_Subtype_4003(
			Model.Class target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<Model.diagram.part.TestLinkDescriptor> result = new LinkedList<Model.diagram.part.TestLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == Model.ModelPackage.eINSTANCE
					.getGenSpec_Subtype()) {
				result.add(new Model.diagram.part.TestLinkDescriptor(
						setting.getEObject(),
						target,
						Model.diagram.providers.TestElementTypes.GenSpecSubtype_4003,
						Model.diagram.edit.parts.GenSpecSubtypeEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<Model.diagram.part.TestLinkDescriptor> getOutgoingFeatureModelFacetLinks_GenSpec_Supertype_4002(
			Model.GenSpec source) {
		LinkedList<Model.diagram.part.TestLinkDescriptor> result = new LinkedList<Model.diagram.part.TestLinkDescriptor>();
		Model.Class destination = source.getSupertype();
		if (destination == null) {
			return result;
		}
		result.add(new Model.diagram.part.TestLinkDescriptor(source,
				destination,
				Model.diagram.providers.TestElementTypes.GenSpecSupertype_4002,
				Model.diagram.edit.parts.GenSpecSupertypeEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<Model.diagram.part.TestLinkDescriptor> getOutgoingFeatureModelFacetLinks_GenSpec_Subtype_4003(
			Model.GenSpec source) {
		LinkedList<Model.diagram.part.TestLinkDescriptor> result = new LinkedList<Model.diagram.part.TestLinkDescriptor>();
		for (Iterator<?> destinations = source.getSubtype().iterator(); destinations
				.hasNext();) {
			Model.Class destination = (Model.Class) destinations.next();
			result.add(new Model.diagram.part.TestLinkDescriptor(
					source,
					destination,
					Model.diagram.providers.TestElementTypes.GenSpecSubtype_4003,
					Model.diagram.edit.parts.GenSpecSubtypeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<Model.diagram.part.TestNodeDescriptor> getSemanticChildren(
				View view) {
			return TestDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<Model.diagram.part.TestLinkDescriptor> getContainedLinks(
				View view) {
			return TestDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<Model.diagram.part.TestLinkDescriptor> getIncomingLinks(
				View view) {
			return TestDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<Model.diagram.part.TestLinkDescriptor> getOutgoingLinks(
				View view) {
			return TestDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
