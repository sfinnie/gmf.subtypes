package Model.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String TestCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String TestCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TestCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TestCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TestCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TestCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String TestCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String TestCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String TestDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String TestDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String TestDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String TestDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String TestDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String TestDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String TestDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String TestDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String TestDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String TestElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Model1Group_title;

	/**
	 * @generated
	 */
	public static String Class1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Class1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String GenSpec2CreationTool_title;

	/**
	 * @generated
	 */
	public static String GenSpec2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String GenSpecSupertype3CreationTool_title;

	/**
	 * @generated
	 */
	public static String GenSpecSupertype3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String GenSpecSubtype4CreationTool_title;

	/**
	 * @generated
	 */
	public static String GenSpecSubtype4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Domain_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Class_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_GenSpec_2002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_GenSpecSupertype_4002_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_GenSpecSupertype_4002_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_GenSpecSubtype_4003_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_GenSpecSubtype_4003_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String TestModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String TestModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
