package Model.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.update.UpdaterLinkDescriptor;

/**
 * @generated
 */
public class TestLinkDescriptor extends UpdaterLinkDescriptor {
	/**
	 * @generated
	 */
	public TestLinkDescriptor(EObject source, EObject destination,
			IElementType elementType, int linkVID) {
		super(source, destination, elementType, linkVID);
	}

	/**
	 * @generated
	 */
	public TestLinkDescriptor(EObject source, EObject destination,
			EObject linkElement, IElementType elementType, int linkVID) {
		super(source, destination, linkElement, elementType, linkVID);
	}

}
