package Model.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class TestVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "gmf.test.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (Model.diagram.edit.parts.DomainEditPart.MODEL_ID.equals(view
					.getType())) {
				return Model.diagram.edit.parts.DomainEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return Model.diagram.part.TestVisualIDRegistry.getVisualID(view
				.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				Model.diagram.part.TestDiagramEditorPlugin.getInstance()
						.logError(
								"Unable to parse view type as a visualID number: "
										+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (Model.ModelPackage.eINSTANCE.getDomain().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((Model.Domain) domainElement)) {
			return Model.diagram.edit.parts.DomainEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = Model.diagram.part.TestVisualIDRegistry
				.getModelID(containerView);
		if (!Model.diagram.edit.parts.DomainEditPart.MODEL_ID
				.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (Model.diagram.edit.parts.DomainEditPart.MODEL_ID
				.equals(containerModelID)) {
			containerVisualID = Model.diagram.part.TestVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = Model.diagram.edit.parts.DomainEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case Model.diagram.edit.parts.DomainEditPart.VISUAL_ID:
			if (Model.ModelPackage.eINSTANCE.getClass_().isSuperTypeOf(
					domainElement.eClass())) {
				return Model.diagram.edit.parts.ClassEditPart.VISUAL_ID;
			}
			if (Model.ModelPackage.eINSTANCE.getGenSpec().isSuperTypeOf(
					domainElement.eClass())) {
				return Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = Model.diagram.part.TestVisualIDRegistry
				.getModelID(containerView);
		if (!Model.diagram.edit.parts.DomainEditPart.MODEL_ID
				.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (Model.diagram.edit.parts.DomainEditPart.MODEL_ID
				.equals(containerModelID)) {
			containerVisualID = Model.diagram.part.TestVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = Model.diagram.edit.parts.DomainEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case Model.diagram.edit.parts.DomainEditPart.VISUAL_ID:
			if (Model.diagram.edit.parts.ClassEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Model.diagram.edit.parts.ClassEditPart.VISUAL_ID:
			if (Model.diagram.edit.parts.ClassNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID:
			if (Model.diagram.edit.parts.GenSpecNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(Model.Domain element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case Model.diagram.edit.parts.DomainEditPart.VISUAL_ID:
			return false;
		case Model.diagram.edit.parts.ClassEditPart.VISUAL_ID:
		case Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return Model.diagram.part.TestVisualIDRegistry.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return Model.diagram.part.TestVisualIDRegistry.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return Model.diagram.part.TestVisualIDRegistry.getNodeVisualID(
					containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return Model.diagram.part.TestVisualIDRegistry.checkNodeVisualID(
					containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return Model.diagram.part.TestVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return Model.diagram.part.TestVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
