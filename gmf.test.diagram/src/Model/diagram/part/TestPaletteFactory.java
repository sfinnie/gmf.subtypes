package Model.diagram.part;

import java.util.Collections;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

/**
 * @generated
 */
public class TestPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createModel1Group());
	}

	/**
	 * Creates "Model" palette tool group
	 * @generated
	 */
	private PaletteContainer createModel1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Model.diagram.part.Messages.Model1Group_title);
		paletteContainer.setId("createModel1Group"); //$NON-NLS-1$
		paletteContainer.add(createClass1CreationTool());
		paletteContainer.add(createGenSpec2CreationTool());
		paletteContainer.add(createGenSpecSupertype3CreationTool());
		paletteContainer.add(createGenSpecSubtype4CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createClass1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Model.diagram.part.Messages.Class1CreationTool_title,
				Model.diagram.part.Messages.Class1CreationTool_desc,
				Collections
						.singletonList(Model.diagram.providers.TestElementTypes.Class_2001));
		entry.setId("createClass1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(Model.diagram.providers.TestElementTypes
				.getImageDescriptor(Model.diagram.providers.TestElementTypes.Class_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createGenSpec2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Model.diagram.part.Messages.GenSpec2CreationTool_title,
				Model.diagram.part.Messages.GenSpec2CreationTool_desc,
				Collections
						.singletonList(Model.diagram.providers.TestElementTypes.GenSpec_2002));
		entry.setId("createGenSpec2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(Model.diagram.providers.TestElementTypes
				.getImageDescriptor(Model.diagram.providers.TestElementTypes.GenSpec_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createGenSpecSupertype3CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Model.diagram.part.Messages.GenSpecSupertype3CreationTool_title,
				Model.diagram.part.Messages.GenSpecSupertype3CreationTool_desc,
				Collections
						.singletonList(Model.diagram.providers.TestElementTypes.GenSpecSupertype_4002));
		entry.setId("createGenSpecSupertype3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(Model.diagram.providers.TestElementTypes
				.getImageDescriptor(Model.diagram.providers.TestElementTypes.GenSpecSupertype_4002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createGenSpecSubtype4CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Model.diagram.part.Messages.GenSpecSubtype4CreationTool_title,
				Model.diagram.part.Messages.GenSpecSubtype4CreationTool_desc,
				Collections
						.singletonList(Model.diagram.providers.TestElementTypes.GenSpecSubtype_4003));
		entry.setId("createGenSpecSubtype4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(Model.diagram.providers.TestElementTypes
				.getImageDescriptor(Model.diagram.providers.TestElementTypes.GenSpecSubtype_4003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
