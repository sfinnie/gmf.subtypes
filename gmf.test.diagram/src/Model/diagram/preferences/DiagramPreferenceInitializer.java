package Model.diagram.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * @generated
 */
public class DiagramPreferenceInitializer extends AbstractPreferenceInitializer {

	/**
	 * @generated
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = getPreferenceStore();
		Model.diagram.preferences.DiagramGeneralPreferencePage
				.initDefaults(store);
		Model.diagram.preferences.DiagramAppearancePreferencePage
				.initDefaults(store);
		Model.diagram.preferences.DiagramConnectionsPreferencePage
				.initDefaults(store);
		Model.diagram.preferences.DiagramPrintingPreferencePage
				.initDefaults(store);
		Model.diagram.preferences.DiagramRulersAndGridPreferencePage
				.initDefaults(store);

	}

	/**
	 * @generated
	 */
	protected IPreferenceStore getPreferenceStore() {
		return Model.diagram.part.TestDiagramEditorPlugin.getInstance()
				.getPreferenceStore();
	}
}
