package Model.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

/**
 * @generated
 */
public class GenSpecSubtypeItemSemanticEditPolicy extends
		Model.diagram.edit.policies.TestBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public GenSpecSubtypeItemSemanticEditPolicy() {
		super(Model.diagram.providers.TestElementTypes.GenSpecSubtype_4003);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
