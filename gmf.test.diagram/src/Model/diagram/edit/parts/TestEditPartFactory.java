package Model.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;

/**
 * @generated
 */
public class TestEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (Model.diagram.part.TestVisualIDRegistry.getVisualID(view)) {

			case Model.diagram.edit.parts.DomainEditPart.VISUAL_ID:
				return new Model.diagram.edit.parts.DomainEditPart(view);

			case Model.diagram.edit.parts.ClassEditPart.VISUAL_ID:
				return new Model.diagram.edit.parts.ClassEditPart(view);

			case Model.diagram.edit.parts.ClassNameEditPart.VISUAL_ID:
				return new Model.diagram.edit.parts.ClassNameEditPart(view);

			case Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID:
				return new Model.diagram.edit.parts.GenSpecEditPart(view);

			case Model.diagram.edit.parts.GenSpecNameEditPart.VISUAL_ID:
				return new Model.diagram.edit.parts.GenSpecNameEditPart(view);

			case Model.diagram.edit.parts.GenSpecSupertypeEditPart.VISUAL_ID:
				return new Model.diagram.edit.parts.GenSpecSupertypeEditPart(
						view);

			case Model.diagram.edit.parts.GenSpecSubtypeEditPart.VISUAL_ID:
				return new Model.diagram.edit.parts.GenSpecSubtypeEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}

}
