package Model.diagram.navigator;

import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

/**
 * @generated
 */
public class TestNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		Model.diagram.part.TestDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		Model.diagram.part.TestDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof Model.diagram.navigator.TestNavigatorItem
				&& !isOwnView(((Model.diagram.navigator.TestNavigatorItem) element)
						.getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof Model.diagram.navigator.TestNavigatorGroup) {
			Model.diagram.navigator.TestNavigatorGroup group = (Model.diagram.navigator.TestNavigatorGroup) element;
			return Model.diagram.part.TestDiagramEditorPlugin.getInstance()
					.getBundledImage(group.getIcon());
		}

		if (element instanceof Model.diagram.navigator.TestNavigatorItem) {
			Model.diagram.navigator.TestNavigatorItem navigatorItem = (Model.diagram.navigator.TestNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (Model.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Model.diagram.edit.parts.DomainEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://www.eclipse.org/gmftest?Domain", Model.diagram.providers.TestElementTypes.Domain_1000); //$NON-NLS-1$
		case Model.diagram.edit.parts.ClassEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://www.eclipse.org/gmftest?Class", Model.diagram.providers.TestElementTypes.Class_2001); //$NON-NLS-1$
		case Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://www.eclipse.org/gmftest?GenSpec", Model.diagram.providers.TestElementTypes.GenSpec_2002); //$NON-NLS-1$
		case Model.diagram.edit.parts.GenSpecSupertypeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://www.eclipse.org/gmftest?GenSpec?supertype", Model.diagram.providers.TestElementTypes.GenSpecSupertype_4002); //$NON-NLS-1$
		case Model.diagram.edit.parts.GenSpecSubtypeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://www.eclipse.org/gmftest?GenSpec?subtype", Model.diagram.providers.TestElementTypes.GenSpecSubtype_4003); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = Model.diagram.part.TestDiagramEditorPlugin
				.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null
				&& elementType != null
				&& Model.diagram.providers.TestElementTypes
						.isKnownElementType(elementType)) {
			image = Model.diagram.providers.TestElementTypes
					.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof Model.diagram.navigator.TestNavigatorGroup) {
			Model.diagram.navigator.TestNavigatorGroup group = (Model.diagram.navigator.TestNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof Model.diagram.navigator.TestNavigatorItem) {
			Model.diagram.navigator.TestNavigatorItem navigatorItem = (Model.diagram.navigator.TestNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (Model.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Model.diagram.edit.parts.DomainEditPart.VISUAL_ID:
			return getDomain_1000Text(view);
		case Model.diagram.edit.parts.ClassEditPart.VISUAL_ID:
			return getClass_2001Text(view);
		case Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID:
			return getGenSpec_2002Text(view);
		case Model.diagram.edit.parts.GenSpecSupertypeEditPart.VISUAL_ID:
			return getGenSpecSupertype_4002Text(view);
		case Model.diagram.edit.parts.GenSpecSubtypeEditPart.VISUAL_ID:
			return getGenSpecSubtype_4003Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getDomain_1000Text(View view) {
		Model.Domain domainModelElement = (Model.Domain) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			Model.diagram.part.TestDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getClass_2001Text(View view) {
		IParser parser = Model.diagram.providers.TestParserProvider
				.getParser(
						Model.diagram.providers.TestElementTypes.Class_2001,
						view.getElement() != null ? view.getElement() : view,
						Model.diagram.part.TestVisualIDRegistry
								.getType(Model.diagram.edit.parts.ClassNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			Model.diagram.part.TestDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getGenSpec_2002Text(View view) {
		IParser parser = Model.diagram.providers.TestParserProvider
				.getParser(
						Model.diagram.providers.TestElementTypes.GenSpec_2002,
						view.getElement() != null ? view.getElement() : view,
						Model.diagram.part.TestVisualIDRegistry
								.getType(Model.diagram.edit.parts.GenSpecNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			Model.diagram.part.TestDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getGenSpecSupertype_4002Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getGenSpecSubtype_4003Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return Model.diagram.edit.parts.DomainEditPart.MODEL_ID
				.equals(Model.diagram.part.TestVisualIDRegistry
						.getModelID(view));
	}

}
