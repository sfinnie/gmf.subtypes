package Model.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

/**
 * @generated
 */
public class TestNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 4005;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof Model.diagram.navigator.TestNavigatorItem) {
			Model.diagram.navigator.TestNavigatorItem item = (Model.diagram.navigator.TestNavigatorItem) element;
			return Model.diagram.part.TestVisualIDRegistry.getVisualID(item
					.getView());
		}
		return GROUP_CATEGORY;
	}

}
