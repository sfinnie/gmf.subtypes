package Model.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

/**
 * @generated
 */
public class TestElementTypes {

	/**
	 * @generated
	 */
	private TestElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			Model.diagram.part.TestDiagramEditorPlugin.getInstance()
					.getItemProvidersAdapterFactory());

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType Domain_1000 = getElementType("gmf.test.diagram.Domain_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Class_2001 = getElementType("gmf.test.diagram.Class_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType GenSpec_2002 = getElementType("gmf.test.diagram.GenSpec_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType GenSpecSupertype_4002 = getElementType("gmf.test.diagram.GenSpecSupertype_4002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType GenSpecSubtype_4003 = getElementType("gmf.test.diagram.GenSpecSubtype_4003"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(Domain_1000, Model.ModelPackage.eINSTANCE.getDomain());

			elements.put(Class_2001, Model.ModelPackage.eINSTANCE.getClass_());

			elements.put(GenSpec_2002,
					Model.ModelPackage.eINSTANCE.getGenSpec());

			elements.put(GenSpecSupertype_4002,
					Model.ModelPackage.eINSTANCE.getGenSpec_Supertype());

			elements.put(GenSpecSubtype_4003,
					Model.ModelPackage.eINSTANCE.getGenSpec_Subtype());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(Domain_1000);
			KNOWN_ELEMENT_TYPES.add(Class_2001);
			KNOWN_ELEMENT_TYPES.add(GenSpec_2002);
			KNOWN_ELEMENT_TYPES.add(GenSpecSupertype_4002);
			KNOWN_ELEMENT_TYPES.add(GenSpecSubtype_4003);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case Model.diagram.edit.parts.DomainEditPart.VISUAL_ID:
			return Domain_1000;
		case Model.diagram.edit.parts.ClassEditPart.VISUAL_ID:
			return Class_2001;
		case Model.diagram.edit.parts.GenSpecEditPart.VISUAL_ID:
			return GenSpec_2002;
		case Model.diagram.edit.parts.GenSpecSupertypeEditPart.VISUAL_ID:
			return GenSpecSupertype_4002;
		case Model.diagram.edit.parts.GenSpecSubtypeEditPart.VISUAL_ID:
			return GenSpecSubtype_4003;
		}
		return null;
	}

	/**
	 * @generated
	 */
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(
			elementTypeImages) {

		/**
		 * @generated
		 */
		@Override
		public boolean isKnownElementType(IElementType elementType) {
			return Model.diagram.providers.TestElementTypes
					.isKnownElementType(elementType);
		}

		/**
		 * @generated
		 */
		@Override
		public IElementType getElementTypeForVisualId(int visualID) {
			return Model.diagram.providers.TestElementTypes
					.getElementType(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public ENamedElement getDefiningNamedElement(
				IAdaptable elementTypeAdapter) {
			return Model.diagram.providers.TestElementTypes
					.getElement(elementTypeAdapter);
		}
	};

}
