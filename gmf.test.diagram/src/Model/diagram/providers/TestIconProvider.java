package Model.diagram.providers;

import org.eclipse.gmf.runtime.common.ui.services.icon.IIconProvider;
import org.eclipse.gmf.tooling.runtime.providers.DefaultElementTypeIconProvider;

/**
 * @generated
 */
public class TestIconProvider extends DefaultElementTypeIconProvider implements
		IIconProvider {

	/**
	 * @generated
	 */
	public TestIconProvider() {
		super(Model.diagram.providers.TestElementTypes.TYPED_INSTANCE);
	}

}
