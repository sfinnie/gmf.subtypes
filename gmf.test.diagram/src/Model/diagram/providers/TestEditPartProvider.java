package Model.diagram.providers;

import org.eclipse.gmf.tooling.runtime.providers.DefaultEditPartProvider;

/**
 * @generated
 */
public class TestEditPartProvider extends DefaultEditPartProvider {

	/**
	 * @generated
	 */
	public TestEditPartProvider() {
		super(new Model.diagram.edit.parts.TestEditPartFactory(),
				Model.diagram.part.TestVisualIDRegistry.TYPED_INSTANCE,
				Model.diagram.edit.parts.DomainEditPart.MODEL_ID);
	}

}
