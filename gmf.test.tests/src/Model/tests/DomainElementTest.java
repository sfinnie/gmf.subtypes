/**
 */
package Model.tests;

import Model.DomainElement;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Domain Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DomainElementTest extends TestCase {

	/**
	 * The fixture for this Domain Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DomainElement fixture = null;

	/**
	 * Constructs a new Domain Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Domain Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DomainElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Domain Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DomainElement getFixture() {
		return fixture;
	}

} //DomainElementTest
